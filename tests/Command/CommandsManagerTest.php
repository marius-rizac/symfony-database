<?php

namespace Kisphp\Tests\Command;

use Kisphp\DatabaseBundle\Command\CommandsManager;
use PHPUnit\Framework\TestCase;

class CommandsManagerTest extends TestCase
{
    /**
     * @var array
     */
    protected $connection = [
        'host' => 'localhost',
        'user' => 'user',
        'password' => 'pass',
        'dbname' => 'dbname',
    ];

    /**
     * @var string
     */
    protected $rootDir = 'tmp/dir';

    public function testImportCommands()
    {
        $cm = new CommandsManager($this->connection, $this->rootDir);

        $this->assertSame(
            $this->getExpectedImportCommand(),
            $cm->getImportCommand()
        );
    }

    public function testExportCommands()
    {
        $cm = new CommandsManager($this->connection, $this->rootDir);

        $this->assertSame(
            $this->getExpectedExportCommand(),
            $cm->getExportCommand()
        );
    }

    public function testImportCommandsWithFileParam()
    {
        $cm = new CommandsManager($this->connection, $this->rootDir,'export.sql');

        $this->assertSame(
            $this->getExpectedImportCommand('export.sql'),
            $cm->getImportCommand()
        );
    }

    public function testExportCommandsWithFileParam()
    {
        $cm = new CommandsManager($this->connection, $this->rootDir, 'export.sql');

        $this->assertSame(
            $this->getExpectedExportCommand('export.sql'),
            $cm->getExportCommand()
        );
    }

    /**
     * @param string $filename
     *
     * @return string
     */
    protected function getExpectedImportCommand($filename=null)
    {
        $file = 'dbname.sql';
        if (!empty($filename)) {
            $file = $filename;
        }

        $expected = vsprintf(
            '/usr/bin/mysql -h%s -u%s -p%s %s < %s/' . $file,
            $this->getConfig()
        );

        return $expected;
    }

    /**
     * @param string $filename
     *
     * @return string
     */
    protected function getExpectedExportCommand($filename=null)
    {
        $file = 'dbname.sql';
        if (!empty($filename)) {
            $file = $filename;
        }

        $expected = vsprintf(
            '/usr/bin/mysqldump -h%s -u%s -p%s %s > %s/' . $file,
            $this->getConfig()
        );

        return $expected;
    }

    protected function getConfig()
    {
        return array_merge($this->connection, [$this->rootDir]);
    }
}
