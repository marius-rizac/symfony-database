.PONY: i t it

COMPOSER = $(shell which composer)
PHPUNIT = vendor/bin/phpunit

it: i t

i:
	$(COMPOSER) install -o -a

t:
	XDEBUG_MODE=coverage $(PHPUNIT)
