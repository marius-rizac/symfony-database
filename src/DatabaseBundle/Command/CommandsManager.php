<?php

namespace Kisphp\DatabaseBundle\Command;

class CommandsManager
{
    /**
     * @var array
     */
    protected $connection = [];

    /**
     * @var string
     */
    protected $rootDirectoryPath;

    /**
     * @var string
     */
    protected $filename;

    /**
     * @param array $connection
     * @param string $rootDirectoryPath
     * @param string $filename
     */
    public function __construct(array $connection, $rootDirectoryPath, $filename = null)
    {
        $this->connection = $connection;
        $this->rootDirectoryPath = $rootDirectoryPath;
        $this->filename = $filename;
    }

    /**
     * @return string
     */
    public function getImportCommand()
    {
        return $this->translateCommand('/usr/bin/mysql -h:host -u:user -p:pass :name < :path/:file');
    }

    /**
     * @return string
     */
    public function getExportCommand()
    {
        return $this->translateCommand('/usr/bin/mysqldump -h:host -u:user -p:pass :name > :path/:file');
    }

    /**
     * @param string $command
     *
     * @return string
     */
    protected function translateCommand($command)
    {
        return str_replace(
            array_keys($this->getDictionary()),
            $this->getDictionary(),
            $command
        );
    }

    /**
     * @return array
     */
    protected function getDictionary()
    {
        $dictionary = [
            ':user' => $this->connection['user'],
            ':pass' => $this->connection['password'],
            ':host' => $this->connection['host'],
            ':name' => $this->connection['dbname'],
            ':file' => $this->connection['dbname'] . '.sql',
            ':path' => $this->rootDirectoryPath,
        ];

        if (!empty($this->filename)) {
            $dictionary[':file'] = $this->filename;
        }

        return $dictionary;
    }
}
