<?php

namespace Kisphp\DatabaseBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Process\Process;

class DatabaseCommand extends ContainerAwareCommand
{
    const RETURN_CODE_NO_ACTION = 1;
    const RETURN_CODE_SUCCESS = 0;

    const OPTION_IMPORT = 'import';
    const OPTION_EXPORT = 'export';

    protected function configure()
    {
        $this->setName('zed:database')
            ->setDescription('Import/Export database sql')
            ->addArgument('file', InputArgument::OPTIONAL, 'File to import')
            ->addOption(self::OPTION_IMPORT, null, InputOption::VALUE_NONE, 'Import Database')
            ->addOption(self::OPTION_EXPORT, null, InputOption::VALUE_NONE, 'Export Database')
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $connection = $this->getContainer()->get('doctrine')->getConnection()->getParams();

        $rootDir = dirname($this->getContainer()->get('kernel')->getRootDIr());

        return $this->showOutputStatus($input, $output, $connection, $rootDir);
    }

    /**
     * @param OutputInterface $output
     * @param array $connection
     * @param string $rootDir
     *
     * @return int
     */
    protected function importDatabaseAction(InputInterface $input, OutputInterface $output, array $connection, $rootDir)
    {
        $output->writeln('<info>Import Database</info>');

        $commandManager = new CommandsManager($connection, $rootDir, $input->getArgument('file'));
        $process = new Process($commandManager->getImportCommand());
        $process->run();

        $output->writeln($process->getOutput());

        return self::RETURN_CODE_SUCCESS;
    }

    /**
     * @param OutputInterface $output
     * @param array $connection
     * @param string $rootDir
     *
     * @return int
     */
    protected function exportDatabaseAction(InputInterface $input, OutputInterface $output, array $connection, $rootDir)
    {
        $output->writeln('<info>Export Database</info>');

        $commandManager = new CommandsManager($connection, $rootDir, $input->getArgument('file'));
        $process = new Process($commandManager->getExportCommand());
        $process->run();

        $output->writeln($process->getOutput());

        return self::RETURN_CODE_SUCCESS;
    }

    /**
     * @param OutputInterface $output
     *
     * @return int
     */
    protected function helpDatabaseAction(OutputInterface $output)
    {
        $output->writeln('<error>ATTENTION:</error> This operation should not be executed in a production environment.');
        $output->writeln('');
        $output->writeln('Please run the operation with <info>--import</info> or <info>--export</info> to execute');

        return self::RETURN_CODE_NO_ACTION;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @param array $connection
     * @param string $rootDir
     *
     * @return int
     */
    protected function showOutputStatus(InputInterface $input, OutputInterface $output, array $connection, $rootDir)
    {
        if ($input->getOption(self::OPTION_IMPORT)) {
            return $this->importDatabaseAction($input, $output, $connection, $rootDir);
        }

        if ($input->getOption(self::OPTION_EXPORT)) {
            return $this->exportDatabaseAction($input, $output, $connection, $rootDir);
        }

        return $this->helpDatabaseAction($output);
    }
}
