# Symfony Database Import/Export 

## Installation

```sh
composer require kisphp/symfony-database:~2.0
```

Register new bundle in AppKernel.php file in your symfony2 project

```php
$bundles = array(
    ...
    new Kisphp\DatabaseBundle\KisphpDatabaseBundle(),
);
```

## Usage:

Import `database_name.sql` file

```sh
php app/console zed:database --import
```

Export current database to `database_name.sql` file

```sh
php app/console zed:database --export
```

# Changelog

### 2.0.0
- support symfony >= 3.x
- support php 7.1+

### 1.1.0
- support symfony >= 2.8, 3.x
